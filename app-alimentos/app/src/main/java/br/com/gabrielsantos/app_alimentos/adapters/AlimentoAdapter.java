package br.com.gabrielsantos.app_alimentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.gabrielsantos.app_alimentos.IAlimentoClickItem;
import br.com.gabrielsantos.app_alimentos.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gabrielsantos.app_alimentos.R;
import br.com.gabrielsantos.app_alimentos.models.Alimento;
import br.com.gabrielsantos.app_alimentos.viewholder.AlimentoViewHolder;

public class AlimentoAdapter extends RecyclerView.Adapter<AlimentoViewHolder> {

    private List<Alimento> alimentos;
    private IAlimentoClickItem iAlimentoClickItem;

    public AlimentoAdapter(List<Alimento> alimentos, IAlimentoClickItem alimentoClickItem) {
        this.alimentos = alimentos;
        this.iAlimentoClickItem = alimentoClickItem;
    }

    @NonNull
    @Override
    public AlimentoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // método que cria o item da RecyclerView
        Context context = parent.getContext(); // obtendo o contexto
        LayoutInflater layoutInflater = LayoutInflater.from(context); // definindo o inflater
        View view = layoutInflater.inflate(R.layout.item_alimento, parent, false);

        return new AlimentoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlimentoViewHolder holder, int position) {
        Alimento alimento = this.alimentos.get(position);
        holder.definirElementos(alimento, this.iAlimentoClickItem);
    }

    @Override
    public int getItemCount() {
        // método que retorna a quantidade de itens da RecyclerView
        return this.alimentos.size(); // retornando a quantidade de alimentos que têm na lista
    }
}
