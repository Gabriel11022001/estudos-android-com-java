package br.com.gabrielsantos.app_alimentos;

import androidx.annotation.NonNull;
import androidx.annotation.StringDef;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import br.com.gabrielsantos.app_alimentos.models.Alimento;
import br.com.gabrielsantos.app_alimentos.viewholder.DetalhesActivityViewHolder;

public class DetalhesActivity extends AppCompatActivity {

    private DetalhesActivityViewHolder detalhesActivityViewHolder;
    private int idAlimento;
    private String nomeAlimento;
    private Double caloriaAlimento;
    private String descricaoAlimento;
    private int quantidadeUnidades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);
        this.detalhesActivityViewHolder = new DetalhesActivityViewHolder();
        // Mapeando os elementos da activity
        this.detalhesActivityViewHolder.textViewNomeAlimento = findViewById(R.id.textView_nome);
        this.detalhesActivityViewHolder.textViewCaloriaAlimento = findViewById(R.id.textView_caloria);
        this.detalhesActivityViewHolder.textDescricaoProduto = findViewById(R.id.textView_descricao_alimento);
        this.detalhesActivityViewHolder.textQuantidadeUnidadesProduto = findViewById(R.id.textView_quantidade_unidades);
        // pegando os dados enviados pela MainActivity
        Bundle bundle = getIntent().getExtras();
        String nome = bundle.getString("nome");
        Double calorias = bundle.getDouble("caloria");
        int id = bundle.getInt("id");
        int quantidadeUnidades = bundle.getInt("quantidade_unidades_alimento");
        String descricaoAlimento = bundle.getString("descricao");
        this.detalhesActivityViewHolder.textViewNomeAlimento.setText(nome);
        this.detalhesActivityViewHolder.textViewCaloriaAlimento.setText(calorias.toString());
        this.detalhesActivityViewHolder.textDescricaoProduto.setText(descricaoAlimento);
        this.detalhesActivityViewHolder.textQuantidadeUnidadesProduto.setText(String.valueOf(quantidadeUnidades));
        // definindo a setinha de retorno no menu
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            // Log.i("TESTE", "Clicou");
            // Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            // startActivity(intent);
            // finishAffinity();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}