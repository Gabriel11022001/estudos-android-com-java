package br.com.gabrielsantos.app_alimentos.viewholder;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gabrielsantos.app_alimentos.DetalhesActivity;
import br.com.gabrielsantos.app_alimentos.IAlimentoClickItem;
import br.com.gabrielsantos.app_alimentos.R;
import br.com.gabrielsantos.app_alimentos.models.Alimento;

public class AlimentoViewHolder extends RecyclerView.ViewHolder {

    private TextView textViewNomeAlimento;
    private TextView textViewCaloriasAlimento;
    private TextView textDescricaoAlimento;
    private TextView textQuantidadeUnidadesAlimento;
    private RelativeLayout relativeLayoutItemAlimento;
    private IAlimentoClickItem alimentoClickItem;

    public AlimentoViewHolder(@NonNull View itemView) {
        super(itemView);
        this.textViewNomeAlimento = itemView.findViewById(R.id.textView_nome);
        this.textViewCaloriasAlimento = itemView.findViewById(R.id.textView_caloria);
        this.relativeLayoutItemAlimento = itemView.findViewById(R.id.linearLayout_item_alimento);
    }

    public void definirElementos(Alimento alimento, IAlimentoClickItem alimentoClickItem) {
        this.alimentoClickItem = alimentoClickItem;
        this.textViewNomeAlimento.setText(alimento.getNome());
        this.textViewCaloriasAlimento.setText(alimento.getCalorias().toString());
        // definindo o evento de click do item
        this.relativeLayoutItemAlimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alimentoClickItem.click(alimento);
            }
        });
    }
}
