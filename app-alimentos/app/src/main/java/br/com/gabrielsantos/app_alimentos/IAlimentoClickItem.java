package br.com.gabrielsantos.app_alimentos;

import br.com.gabrielsantos.app_alimentos.models.Alimento;

public interface IAlimentoClickItem {

    void click(Alimento alimento);
}
