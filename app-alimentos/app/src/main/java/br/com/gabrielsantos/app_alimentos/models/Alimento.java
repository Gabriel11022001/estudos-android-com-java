package br.com.gabrielsantos.app_alimentos.models;

public class Alimento {

    private int id;
    private String nome;
    private Double calorias;
    private int quantidade;
    private String descricao;

    public int getQuantidade() {

        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getDescricao() {

        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Alimento() {}

    public Alimento(int id, String nome, Double calorias, int quantidade, String descricao) {
        this.setId(id);
        this.setNome(nome);
        this.setCalorias(calorias);
        this.setDescricao(descricao);
        this.setQuantidade(quantidade);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getCalorias() {

        return calorias;
    }

    public void setCalorias(Double calorias) {
        this.calorias = calorias;
    }
}
