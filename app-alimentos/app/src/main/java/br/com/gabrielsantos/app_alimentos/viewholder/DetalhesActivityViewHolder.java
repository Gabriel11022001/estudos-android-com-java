package br.com.gabrielsantos.app_alimentos.viewholder;

import android.widget.TextView;

public class DetalhesActivityViewHolder {

    public TextView textViewNomeAlimento;
    public TextView textViewCaloriaAlimento;
    public TextView textDescricaoProduto;
    public TextView textQuantidadeUnidadesProduto;
}
