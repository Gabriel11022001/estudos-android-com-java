package br.com.gabrielsantos.app_alimentos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.gabrielsantos.app_alimentos.adapters.AlimentoAdapter;
import br.com.gabrielsantos.app_alimentos.models.Alimento;
import br.com.gabrielsantos.app_alimentos.viewholder.MainActivityViewHolder;

public class MainActivity extends AppCompatActivity {

    private MainActivityViewHolder mainActivityViewHolder;
    private List<Alimento> alimentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mainActivityViewHolder = new MainActivityViewHolder();
        // Mapeando os elementos da activity
        this.mainActivityViewHolder.recyclerViewAlimentos = findViewById(R.id.recyclerView_alimentos);
        // Definindo a lista de alimentos
        this.alimentos = this.obterAlimentos();
        /**
         * - Entre os dados que vão aparecer na RecyclerView e os dados, eu tenho um
         * adapter, o adapter é o responsável por definir qual será o layout
         * dos elementos da RecyclerView.
         */
        IAlimentoClickItem listener = new IAlimentoClickItem() {
            @Override
            public void click(Alimento alimento) {
                Intent intent = new Intent(getApplicationContext(), DetalhesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", alimento.getId());
                bundle.putDouble("caloria", alimento.getCalorias());
                bundle.putString("nome", alimento.getNome());
                bundle.putInt("quantidade_unidades_alimento", alimento.getQuantidade());
                bundle.putString("descricao", alimento.getDescricao());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        };
        // defindo o adapter dos itens da RecyclerView
        AlimentoAdapter alimentoAdapter = new AlimentoAdapter(this.alimentos, listener);
        this.mainActivityViewHolder.recyclerViewAlimentos.setAdapter(alimentoAdapter);
        // Definindo o layout da RecyclerView
        this.mainActivityViewHolder.recyclerViewAlimentos.setLayoutManager(new LinearLayoutManager(this));
    }

    private List<Alimento> obterAlimentos() {
        List<Alimento> alimentos = new ArrayList<>();

        for (int contador = 0; contador < 10000; contador++) {
            Alimento alimento = new Alimento();
            alimento.setId(contador + 1);
            alimento.setNome("Alimento " + (contador + 1));
            alimento.setQuantidade(100 + contador);
            alimento.setDescricao("Descrição qualquer do alimento");
            alimento.setCalorias(10.0);
            alimentos.add(alimento);
        }

        return alimentos;
    }
}